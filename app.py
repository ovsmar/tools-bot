from distutils.command.build import build
from Modules import *


intents = discord.Intents.default() 
intents.message_content = True

bot = commands.Bot(command_prefix='!', intents=intents) 

#connection BDD
def get_db_connection():
    connection = sqlite3.connect('db/database.db')
    connection.row_factory = sqlite3.Row
    connection.set_trace_callback(print)
    return connection


# Pour convertir une vidéo youtube au format mp3
@bot.command()
async def mp3(ctx,*,message):
    embedOfmp3 = discord.Embed(title =f"{message}")        
    await ctx.message.delete()
    MessageOfmp3 = await ctx.send(embed=embedOfmp3)
    MessageOfdownconverting = await ctx.send("Conversion...")
    await MessageOfmp3.delete()

    yt = YouTube(message)
    video = yt.streams.filter(only_audio=True).first()
    
    destination = "stock"
    out_file = video.download(output_path=destination)
    
    base, ext = os.path.splitext(out_file)
    new_file = base + '.mp3'
    os.rename(out_file, new_file)
    await MessageOfdownconverting.delete()
    MessageOfdownloading = await ctx.send("Téléchargement...")
    await ctx.send(file=discord.File(new_file)) 
    await MessageOfdownloading.delete()
    MessageOfdone = await ctx.send("Terminé, vous pouvez maintenant télécharger et écouter !")
    await asyncio.sleep(3)
    [f.unlink() for f in Path("stock").glob("*") if f.is_file()]
    await MessageOfdone.delete()


#Prévisions météorologiques
@bot.command()
async def weather(ctx, *, city: str):

        city_name = city
        url = f'http://api.openweathermap.org/data/2.5/weather?q={ city }&units=metric&lang=fr&appid='
        complete_url = url + config('API_KEY')
        response = requests.get(complete_url)
        x = response.json()
        channel = ctx.message.channel
        await ctx.message.delete()

        if x["cod"] != "404":

                y = x["main"]
                current_temperature = y["temp"]
                temp_min = y["temp_min"]
                temp_max = y["temp_max"]
                current_pressure = y["pressure"]
                current_humidity = y["humidity"]
                z = x["weather"]
                weather_description = z[0]["description"]
                icon = z[0]["icon"]
               

                embed = discord.Embed(
                    title=f"Prévisions météorologiques - {city_name}  ",
                    color=0x7289DA,
                    timestamp=ctx.message.created_at,
                    
                )
                embed.set_thumbnail(url=f"http://openweathermap.org/img/wn/{icon}.png")

                embed.add_field(name="📄 Description", value=f"**{weather_description}**", inline=False)
                embed.add_field(name="🌡️ Température(C)",value=f"**{current_temperature}°C**",inline=False)
                embed.add_field(name="🔆 Température max(C)", value=f"**{temp_max}°C**", inline=False)
                embed.add_field(name="🔅 Température min(C)", value=f"**{temp_min}°C**", inline=False)
                embed.add_field(name="💧 Humidité(%)", value=f"**{current_humidity}%**", inline=False)
                embed.add_field(name="💨 Pression atmosphérique(hPa)", value=f"**{current_pressure}hPa**", inline=False)
                embed.set_footer(text=f"👁️ Demandé par {ctx.author.name}")

                await channel.send(embed=embed)

        else:
                await channel.send(
                    f"Il n'y avait aucun résultat sur cet endroit !!")   
    

#about
@bot.command()
async def about(ctx):
    await ctx.message.delete()
    async with ctx.typing():
        embedAbout = discord.Embed(title =f"Bonjour!", description ='je suis un Bot qui propose différents outils. Pour obtenir toutes les commandes disponibles de ce bot écrivez "!all"', colour = discord.Colour.from_rgb(0, 204, 102))
        await ctx.send(embed=embedAbout)

#supprimer tous les messages de chat
@bot.command()
@commands.has_permissions(administrator=True)
async def clear(ctx):
  async with ctx.typing():
    await ctx.send("♻️Cleaning!")
    await ctx.channel.purge()
    

#obtenir toutes les commandes disponibles
@bot.command(name="all", description="Toutes les commandes disponibles")
async def all(ctx):
    bot.remove_command('help')
    helptext = "Toutes les commandes disponibles :\n"
    for command in bot.commands:
        helptext+=f"!{command} \n"
    await ctx.message.delete()
    await ctx.send(helptext) 

#suivir le Prix ​​de la crypto-monnaie
bitcoin_api_url = 'https://api.coingecko.com/api/v3/coins/markets?vs_currency=eur'
response = requests.get(bitcoin_api_url)
response_json = response.json()

class Select(discord.ui.Select):
    def __init__(self):
        options=[
            discord.SelectOption(label="Bitcoin",emoji="🪙",),
            discord.SelectOption(label="Ethereum",emoji="🪙"),
            discord.SelectOption(label="Dogecoin",emoji="🪙")
            ]
        super().__init__(placeholder="Select coin",max_values=1,min_values=1,options=options)
    async def callback(self, interaction: discord.Interaction):

    
        if self.values[0] == "Bitcoin":
            await interaction.response.send_message(content='{}: {}€'.format(str(response_json[0]["name"]), str(response_json[0]["current_price"])))
        elif self.values[0] == "Ethereum":
            await interaction.response.send_message(content='{}: {}€'.format(str(response_json[1]["name"]), str(response_json[1]["current_price"])),ephemeral=False)
        elif self.values[0] == "Dogecoin":
            await interaction.response.send_message(content='{}: {}€'.format(str(response_json[9]["name"]), str(response_json[9]["current_price"])),ephemeral=False)

class SelectView(discord.ui.View):
    def __init__(self, *, timeout = 180):
        super().__init__(timeout=timeout)
        self.add_item(Select())

@bot.command()
async def coin(ctx):
    await ctx.send("coin",view=SelectView())
    await ctx.message.delete()

#test
@bot.command()
async def test(ctx):
  Bitcoin =  "https://assets.coingecko.com/coins/images/1/thumb/bitcoin.png?1547033579"
  Ethereum = "https://assets.coingecko.com/coins/images/279/thumb/ethereum.png?1595348880"
  Dogecoin = "https://assets.coingecko.com/coins/images/5/thumb/dogecoin.png?1547792256"
    # file=discord.File(f"{Bitcoin}{Ethereum}{Dogecoin}"
  files = [{"filename": f"{Bitcoin}"}, {"filename": f"{Ethereum}"}, {"filename": f"{Dogecoin}"}]
  await asyncio.wait([ctx.channel.send( f['filename']) for f in files])



#scheduler
@bot.command()
async def message(ctx, message : str, timeout: float): 
    def check(m):
        return m.author == ctx.author and m.channel == ctx.channel

    await ctx.send(message)
    response = await bot.wait_for("message", check=check, timeout=timeout)
    return response.content 
    

def get_text():
      return m
      
      
async def SendMessage():
    messageOfSchedule = str(f"{get_text()}")
    await bot.wait_until_ready()
    channelID = bot.get_channel(1031610330041811067)
    M1 = await channelID.send(messageOfSchedule)
    await asyncio.sleep(13500)
    await M1.delete()
    

@bot.command()
async def schedule(ctx):
    connection = get_db_connection() 
    scheduler = AsyncIOScheduler()
    
    try:
        text = await message(ctx, "Quel est votre message pour schedule ?", timeout=60.0)
        hour = await message(ctx, "Sélectionner l'heure pour schedule? (format: 12,14...)", timeout=60.0)
        minute = await message(ctx, "Sélectionner minutes pour schedule? (format: 30,45...)", timeout=60.0)
        day_of_week = await message(ctx, "sélectionner le jour de la semaine pour  schedule? (format: fri...)", timeout=60.0)

        connection.execute("INSERT INTO schedules (messageOFSchedule, hourOFSchedule, minuteOFSchedule, day_of_weekOFSchedule) VALUES (?,?,?,?)",(text,hour,minute,day_of_week))
        connection.commit()
        # connection.close()

    except asyncio.TimeoutError: 
        await ctx.send("Vous avez été trop lent à répondre !")
    else: 
        await ctx.send(f'message "{text}"\n  heure : "{hour}"\n minutes : "{minute}"\n jour de la semaine : "{day_of_week}"\n ')
        global m
        db_schedules = connection.execute('SELECT messageOFSchedule, hourOFSchedule, minuteOFSchedule, day_of_weekOFSchedule FROM schedules').fetchall()
        connection.close()

        schedules = []
        for schedule in db_schedules:
            schedule = dict(schedule)
            schedules.append(schedule)

        for schedule in schedules:
            m = schedule['messageOFSchedule']
    

        scheduler.add_job(SendMessage, CronTrigger(hour=f"{schedule['hourOFSchedule']}", minute=f"{schedule['minuteOFSchedule']}",day_of_week =f"{schedule['day_of_weekOFSchedule']}"))

        scheduler.start()



@bot.command()
async def button(ctx):
    button = Button(label= "Click me!", style=discord.ButtonStyle.green, emoji="👁️")                                                                      


    async def button_callback(interaction):
        await interaction.response.send_message("hi")

    button.callback = button_callback

    view = View()
    view.add_item(button)
   
    await ctx.send("H1!", view=view)

# pas finis
class myView(View):
    def __init__(self):
        connection = get_db_connection() 
        super().__init__ ()
        

        db_schedules = connection.execute('SELECT id ,messageOFSchedule, hourOFSchedule, minuteOFSchedule, day_of_weekOFSchedule FROM schedules').fetchall()
        connection.close()

        schedules = []
        for schedule in db_schedules:
            schedule = dict(schedule)
            schedules.append(schedule)

        for schedule in schedules:
            self.add_item(Button(label=f"{schedule['id']}",custom_id=f"{schedule['id']}"))
            

@bot.command()
async def delete(ctx):
    await ctx.send(view=myView())
    await ctx.message.delete()


# pour rechercher et afficher des articles de Wikipédia
@bot.command()
async def wiki(ctx,*,message):

    wikipedia.set_lang("fr" )

  
    await ctx.message.delete()
    async with ctx.typing():
        await ctx.send(getwiki(message))


def getwiki(s):
    try:
        ny = wikipedia.page(s)
        # recupere les mille premiers caractères
        wikitext=ny.content[:1000]
        wikimas=wikitext.split('.')
        wikimas = wikimas[:-1]
        wikitext2 = ''
        for x in wikimas:
            if not('==' in x):
                if(len((x.strip()))>3):
                   wikitext2=wikitext2+x+'.'
            else:
                break
        return wikitext2
        #si le bot ne trouve pas le mot
    except Exception as e:
        return "L'encyclopédie n'a aucune information à ce sujet."
                    

@bot.command()
async def creatememe(ctx,*,message):
    embedOfMeme = discord.Embed(title =f"{message}")        
    await ctx.message.delete()
    MessageOfMeme = await ctx.send(embed=embedOfMeme)
    await MessageOfMeme.delete()
    
    username = config('username')
    password = config('password')
    
    #Fetch the available memes
    data = requests.get('https://api.imgflip.com/get_memes').json()['data']['memes']
    images = [{'name':image['name'],'url':image['url'],'id':image['id']} for image in data]
   
    id = choice(range(100))
    text0 = {message}

    #Fetch the generated meme
    URL = 'https://api.imgflip.com/caption_image'
    params = {
        'username':username,
        'password':password,
        'template_id':images[id-1]['id'],
        'text0':text0,
    }
    response = requests.request('POST',URL,params=params).json() 
    await ctx.send(response['data']['url']) 

@bot.command()
async def meme(ctx):      
    await ctx.message.delete()

    content = requests.get("https://meme-api.com/gimme").text
    data = json.loads(content)
    print(data)

    await ctx.send(data['url'])


bot.run(config('TOKEN'))
