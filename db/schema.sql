DROP TABLE IF EXISTS schedules;

CREATE TABLE schedules (
    id INTEGER PRIMARY KEY,
    messageOFSchedule TEXT NOT NULL,
    hourOFSchedule NUMERIC NOT NULL,
    minuteOFSchedule NUMERIC NOT NULL,
    day_of_weekOFSchedule TEXT NOT NULL
);
