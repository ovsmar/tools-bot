# Tools bot

Le projet "Tools Bot" est un bot de Discord développé en utilisant Python et Discord.PY. Ce bot offre plusieurs outils utiles aux utilisateurs de Discord, tels que la possibilité de convertir une vidéo YouTube en format MP3, de obtenir les prévisions météorologiques, de supprimer plusieurs messages de chat en une seule fois, de suivre les prix de différentes crypto-monnaies (comme le Bitcoin, l'Ethereum et le Dogecoin), de planifier des tâches à l'aide d'un outil de planification, de rechercher et afficher des articles de Wikipédia et de créer des memes en ajoutant du texte sur un meme aléatoire. En plus de ces fonctionnalités, le bot offre également la possibilité de générer des memes aléatoires pour le divertissement des utilisateurs. Ce bot est développé en utilisant également sqlite3 pour stocker et gérer les données.

Je suis un Discird.Bot qui propose différents outils:

    -convertir une vidéo youtube au format mp3
    -Prévisions météorologiques
    -supprimer plusieurs messages de chat
    -suivi du prix du de crypto-monnaie (Bitcoin,Ethereum,Dogecoin)
    -scheduler (pas finis)
    -rechercher et afficher des articles de Wikipédia
    -créer un meme en ajoutant un texte avec un meme aléatoire
    -meme aléatoire    
    ...


